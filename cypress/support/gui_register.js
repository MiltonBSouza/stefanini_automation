import Login from '../support/pages/login';
import Pagina from '../support/pages/InforPage';
import Message from '../support/pages/Messages';
import Cadastro from '../support/pages/Cadastro';
import Excluir from '../support/pages/ExcluirCad';


Cypress.Commands.add('gui_register', () => {

    // Iformação de acesso para a página pricipal
    Pagina.acessarPagina()
    Pagina.validPage()

    // Criando uma conta para validação da tablea de Usuários cadastrados.
    // Created an account for validation for the table Usuários cadastrados.
    Login.preencheCadatro()

    // O sistema não pode aceitar a falta de dados nos campos Nome, E-mail e Senha para o Usiário se cadastrar.
    // The fields Nome, E-mail, and Senha must not be empty for the User can make the register.
    Message.MessageError()
    Message.MessageName()
    Message.MessageEmail()
    Message.MessageSenha()
    
    // O Usuario deve se cadastrar com sucesso.
    // The User must be resgister with successes.
    Cadastro.NovoCadatro()
    Cadastro.validPage()

    
    // O cadastro do Usuario deve estar visivel.
    // The User resgister must be visible.
    Cadastro.validPage()
    
    // O Usuario deve poder excluir a conta que criou.
    // The User can exclude an account.

    Excluir.excluirCadastro()

    });