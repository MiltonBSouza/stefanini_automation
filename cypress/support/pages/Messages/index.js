const el = require('./elements').registro;
const faker = require('faker')

class Message {

    // O sistema não pode aceitar a falta de dados nos campos Nome, E-mail e Senha para o Usiário se cadastrar.
    // The fields Nome, E-mail, and Senha must not be empty for the User can make the register.
    MessageError(){
        cy.get(el.botton).click()
        cy.contains('O campo Nome é obrigatório.').should('exist') 
        cy.contains('O campo E-mail é obrigatório.').should('exist') 
        cy.contains('O campo Senha é obrigatório.').should('exist')   
    }


    MessageName(){
    // O sistema não pode aceitar a falta de dados nos campos E-mail e Senha para o Usiário se cadastrar.
    // The fields E-mail, and Senha must not be empty for the User can make the register.
        cy.get(el.name).type(`${faker.name.firstName()} ${faker.name.lastName()}`)
        cy.get(el.botton).click()
        cy.contains('O campo Nome é obrigatório.').should('not.exist') 
        cy.contains('O campo E-mail é obrigatório.').should('exist') 
        cy.contains('O campo Senha é obrigatório.').should('exist') 
        cy.get(el.name).clear() 

    // O sistema não pode aceitar apenas o primeiro nome para o Usiário se cadastrar.
    // The field's name must not accept just the first name for the User can make the register.
        cy.get(el.name).type(`${faker.name.firstName()}`)
        cy.get(el.botton).click()
        cy.contains('Por favor, insira um nome completo.').should('exist') 
        cy.contains('O campo E-mail é obrigatório.').should('exist') 
        cy.contains('O campo Senha é obrigatório.').should('exist') 
        cy.get(el.name).clear()     
    }

    MessageEmail(){
    // O sistema não pode aceitar a falta de dados nos campos Nome e Senha para o Usiário se cadastrar.
    // The fields Nome, and Senha must not be empty for the User can make the register.
        cy.get(el.email).type(faker.internet.email())
        cy.get(el.botton).click()
        cy.contains('O campo Nome é obrigatório.').should('exist') 
        cy.contains('O campo E-mail é obrigatório.').should('not.exist') 
        cy.contains('O campo Senha é obrigatório.').should('exist') 
        cy.get(el.email).clear() 

    // O sistema não pode aceitar um E-mail invalido para o Usiário se cadastrar.
    // The fields E-mail must have a valid e-mail for the User can make the register.
        cy.get(el.email).type(`${faker.name.firstName()}`)
        cy.get(el.botton).click()
        cy.contains('O campo Nome é obrigatório.').should('exist') 
        cy.contains('Por favor, insira um e-mail válido.').should('exist') 
        cy.contains('O campo Senha é obrigatório.').should('exist') 
        cy.get(el.email).clear() 
    }

    MessageSenha(){
    // O sistema não pode aceitar a falta de dados nos campos Nome e E-mail para o Usiário se cadastrar.
    // The fields Nome, and E-amil and must not be empty for the User can make the register.
        cy.get(el.password).type(faker.random.words(8))
        cy.get(el.botton).click()
        cy.contains('O campo Nome é obrigatório.').should('exist') 
        cy.contains('O campo E-mail é obrigatório.').should('exist') 
        cy.contains('O campo Senha é obrigatório.').should('not.exist') 
        cy.get(el.password).clear() 

    // O campo Senha deve ter no minimo 8 caracteres ou mais para o Usiário se cadastrar.
    // The fields Senha must have 8 character or more for the User can make the register.
        cy.get(el.password).type(faker.random.alphaNumeric(4))
        cy.get(el.botton).click()
        cy.contains('O campo Nome é obrigatório.').should('exist') 
        cy.contains('O campo E-mail é obrigatório.').should('exist') 
        cy.contains('A senha deve conter ao menos 8 caracteres.').should('exist') 
        cy.get(el.password).clear() 
    }


}

export default new Message();