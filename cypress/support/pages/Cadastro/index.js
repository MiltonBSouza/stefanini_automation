const el = require('./elements').registro;
const faker = require('faker');

class Cadastro {

    NovoCadatro(){
    // O Usuario deve se cadastrar com sucesso.
    // The User must be resgister with successes.
        cy.get(el.name).type(`${faker.name.firstName()} ${faker.name.lastName()}`)
        cy.get(el.password).type(faker.random.words(8))
        cy.get(el.email).type(faker.internet.email())
        cy.get(el.botton).click()
    }

    
    validPage(){
        // O cadastro do Usuario deve estar visivel.
        // The User account must be visible.
        cy.get(el.cadastrados).should('exist')
        cy.get(el.Cadastro).should('exist')
        cy.get(el.EmailCadastro).should('exist')
        
        cy.contains('O campo Nome é obrigatório.').should('not.exist') 
        cy.contains('O campo E-mail é obrigatório.').should('not.exist') 
        cy.contains('O campo Senha é obrigatório.').should('not.exist') 
        cy.contains('A senha deve conter ao menos 8 caracteres.').should('not.exist') 
        cy.contains('Por favor, insira um e-mail válido.').should('not.exist') 
        cy.contains('Por favor, insira um nome completo.').should('not.exist') 
    }


}

export default new Cadastro();