const el = require('./elements').registro;
const faker = require('faker')

class Login {

    preencheCadatro(){
        cy.get(el.name).type(`${faker.name.firstName()} ${faker.name.lastName()}`)
        cy.get(el.password).type(faker.random.words(8))
        cy.get(el.email).type(faker.internet.email())
        cy.get(el.botton).click()
    }
}

export default new Login();