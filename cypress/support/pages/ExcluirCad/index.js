const el = require('./elements').registro

class Excluir {
    
    excluirCadastro(){
    // O Usuario deve poder excluir a conta que criou.
    // The User can exclude an account.
        cy.get(el.exlcuir).click()

    // A conta que o Usuario excluiu não deve interferir com o ID das outras contas
        cy.contains('Excluir').should('not.equal', el.exlcuir)
        cy.contains('Excluir').click() 
        
     // Excluindo todas as contas e validando a tabela de Úsuarios Cadastrados
        cy.get(el.cadastrados).should('not.exist')
        cy.contains(el.name).should('not.exist') 
        cy.contains(el.email).should('not.exist') 
    }

}

export default new Excluir();