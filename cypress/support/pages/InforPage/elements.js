/// <reference types="Cypress" />

export const registro = {
    name: '#name',
    password: '#password',
    email: '#email',
    botton: '#register',
    formulario: '.register-form',
    cadastrados: '.table-title'
}