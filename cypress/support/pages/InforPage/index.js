const el = require('./elements').registro;

//Validação de customização da página de cadastro.
//Tudo no seu lugar para dar certo! :)

class Pagina {
    // Iformação de acesso para a página pricipal
    acessarPagina(){
        cy.visit('http://prova.stefanini-jgr.com.br/teste/qa/');
    }

    // Informações que a página deve apresentar para o Usuário. 
    // This informations must be appear for the User.
    validPage(){
    cy.contains('Cadastro de usuários').should('exist')
    cy.contains('Para realizar o cadastro de um usuário, insira dados válidos no formulário e acione a opção Cadastrar :)')
    .should('exist')
    cy.contains('Formulário').should('exist')
    cy.get(el.formulario).should('exist')
    cy.get(el.cadastrados).should('not.exist')
    
    // Os campos devem estar sem nenhum caractere e com indicador de texto.
    // The fields must be empty and with the appropriate placeholder.
    cy.get(el.name).should('be.empty')
    cy.get(el.name).should('have.attr', 'placeholder', 'João da Silva')
    cy.get(el.email).should('be.empty')
    cy.get(el.email).should('have.attr', 'placeholder', 'joao.silva@email.com')
    cy.get(el.password).should('be.empty')
    cy.get(el.password).should('have.attr', 'placeholder', '********')
    }
}

export default new Pagina();